# Serasa Experian OEDx
> Base para a produção de webaulas da Serasa Experian, utiliza [](http://markdalgleish.com/projects/bespoke.js) com o gerador [generator-bespoke](https://github.com/markdalgleish/generator-bespoke)

## Instalar

Para rodar, certifique-se de ter instalado em seu sistema as seguintes dependências:

1. [Node.js](http://nodejs.org)
2. [Bower](http://bower.io): `$ npm install -g bower`
3. [Gulp](http://gulpjs.com): `$ npm install -g gulp`

Então, execute os seguintes comandos para instalar as dependências locais e iniciar o webserver:

```bash
$ npm install && bower install
$ gulp serve
```

## Web-aulas

O branch *master* contém a base das atividades. Cada webaula deverá ser criada em um branch diferente, gerado a partir do branch master.

O arquivo index.jade contém todos os slides de uma webaula. Basicamente
O arquivo index.jade contém todos os slides de uma webaula. Basicamente, cada `section(class='card')` é um novo frame da webaula. A referência para entender a sintaxe *jade* está aqui [](http://jade-lang.com)
